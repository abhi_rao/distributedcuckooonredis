package cuckooonredis;

import com.google.common.hash.Hashing;
import com.sun.org.apache.regexp.internal.RE;
import cuckooutil.*;
import org.apache.commons.cli.*;
import redis.clients.jedis.BinaryJedis;

import java.io.*;
import java.util.*;

import static cuckooonredis.CuckooFilter.sendObject;

/**
 * Created by abhijith on 25/2/17.
 */
public class CuckooClient {
    public static long maxTotalKeyCount = 0;
    private Node seedNode;
    public static CuckooClient cuckooClient;
    private static BinaryJedis[] binaryJedises;
    private static long initialTime;
    public CuckooClient(Node seedNode) {
        this.seedNode = seedNode;

        ClusterOps clusterOps = new ClusterOps(FilterCommunication.ClusterOpsEnum.CLUSTER_STATE, null);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.CLUSTEROPS, clusterOps);
        TreeMap<Long, byte[]> serverMap = (TreeMap<Long, byte[]>) sendObject(this.seedNode, filterCommunication);
        HashSet<byte[]> bServerSet = new HashSet<>();
        bServerSet.addAll(serverMap.values());
        binaryJedises = new BinaryJedis[bServerSet.size()];
        try {
            int i = 0;
            for (byte[] val: bServerSet) {
                Node node = (Node) Serializer.deserialize(val);
                binaryJedises[i++] = new BinaryJedis(node.getIpAddress(), node.getPort());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Long hash(byte[] x) {
        return Math.abs(Hashing.murmur3_128().hashBytes(x).asLong()) % CuckooFilter.MAX_FILTER_SIZE;
    }

    public Data getData(String[] parts) throws IOException {
        Object key = parts[1];
        byte[] x = Serializer.serialize(key);
        byte[] f = Hashing.crc32().hashBytes(x).asBytes();
        long i1 = hash(x);
        long i2 = i1 ^ hash(f);
        int retries = 0;
        return new Data(f, i1, i2, retries, new Long(parts[0]));
    }

    public boolean insert(String[] parts) throws IOException {
        Data data = this.getData(parts);
        Message message = new Message(FilterCommunication.MessageEnum.SET, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        return (Boolean) sendObject(this.seedNode, filterCommunication);
    }

    public boolean delete(String[] parts) throws IOException {
        Data data = this.getData(parts);
        Message message = new Message(FilterCommunication.MessageEnum.DELETE, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        return (Boolean) sendObject(this.seedNode, filterCommunication);
    }

    public boolean lookup(String[] parts) throws IOException {
        Data data = this.getData(parts);
        Message message = new Message(FilterCommunication.MessageEnum.EXISTS, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        return (Boolean) sendObject(this.seedNode, filterCommunication);
    }

    public static boolean performOperation(String operation, String[] parts) throws IOException {
        boolean isSuccess = false;
        long timeBefore = System.nanoTime();
        if (operation.equals("INSERT")) {
            isSuccess = cuckooClient.insert(parts);
        }
        if (operation.equals("DELETE")) {
            isSuccess = cuckooClient.delete(parts);
        }
        if (operation.equals("LOOKUP")) {
            isSuccess = cuckooClient.lookup(parts);
        }
        long timeAfter = System.nanoTime();
        recordStats(parts[0], operation, timeAfter - timeBefore, isSuccess);
        return isSuccess;
    }

    private static void recordStats(String index, String operation, long time, boolean isSuccess) throws IOException {
        long memory = 0, dbSize = 0;
        for (BinaryJedis binaryJedis: binaryJedises) {
            dbSize += binaryJedis.dbSize();
            String sMemory = binaryJedis.info("memory").split("\n")[1].split(":")[1];
            sMemory = sMemory.substring(0, sMemory.length() - 1);
            memory += Long.valueOf(sMemory);
        }

        FileWriter fileWriter = new FileWriter(String.format("%s_%d_stats.csv", operation, initialTime), true);
        fileWriter.write(String.format("%s,%s,%s,%s,%s\n", index, dbSize, memory, time, (isSuccess ? "1" : "0")));

        fileWriter.close();
    }

    public static double performOperationForFile(String operation, File file) throws IOException {
        double count = 0, total = 0;
        if (!file.isFile())
            return 0;
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            String[] parts = line.split(",");
            boolean returnValue = performOperation(operation, parts);
            if (returnValue) {
                count++;
            }
            total++;
            if(total%1000 == 0) {
                System.out.println(String.format("count: %f, total: %f", count, total));
            }
            maxTotalKeyCount--;
            if(maxTotalKeyCount <= 0)
                break;
        }
        System.out.println(String.format("count: %f, total: %f", count, total));
        return count / total;
    }

    public static void main(String[] args) throws IOException, ParseException {
        initialTime = System.nanoTime();
        String ipAddress = "127.0.0.1";
        short port = 6379;
        long maxTotalFileCount = Long.MAX_VALUE;
        String operation = "INSERT";
        boolean recursive = false;
        String filePath = ".";
        long cuckooFilterSize = 1<<16;
        Options options = new Options();
        options.addOption("ipAddress", true, "IP address on which Redis server listens");
        options.addOption("port", true, "Port on which Redis server listens");
        options.addOption("recursive", false, "Recursive option");
        options.addOption("operation", true, "INSERT/DELETE/LOOKUP");
        options.addOption("filePath", true, "File path of the source");
        options.addOption("maxTotalFileCount", true, "Maximum total file count");
        options.addOption("maxTotalKeyCount", true, "Maximum total file count");
        options.addOption("cuckooFilterSize", true, "Cuckoo filter size");
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        if (commandLine.hasOption("ipAddress")) {
            ipAddress = commandLine.getOptionValue("ipAddress");
        }
        if (commandLine.hasOption("port")) {
            port = Short.parseShort(commandLine.getOptionValue("port"));
        }
        if (commandLine.hasOption("recursive")) {
            recursive = true;
        }
        if (commandLine.hasOption("operation")) {
            operation = commandLine.getOptionValue("operation");
        }
        if (commandLine.hasOption("filePath")) {
            filePath = commandLine.getOptionValue("filePath");
        }
        if(commandLine.hasOption("cuckooFilterSize")) {
            cuckooFilterSize = Integer.parseInt(commandLine.getOptionValue("cuckooFilterSize"));
            CuckooFilter.MAX_FILTER_SIZE = cuckooFilterSize;

            while(cuckooFilterSize > 0) {
                cuckooFilterSize >>= 1;
            }
        }
        if(commandLine.hasOption("maxTotalFileCount")) {
            maxTotalFileCount = Integer.parseInt(commandLine.getOptionValue("maxTotalFileCount"));

        }
        if(commandLine.hasOption("maxTotalKeyCount")) {
            maxTotalKeyCount = Integer.parseInt(commandLine.getOptionValue("maxTotalKeyCount"));

        }
        double finalAccuracy = 0.0;
        long totalFiles = 0;
        cuckooClient = new CuckooClient(new Node(ipAddress, port));
        File f = new File(filePath);
        if (recursive) {
            if (f.isDirectory()) {
                for (File file : f.listFiles()) {
                    if(totalFiles == maxTotalFileCount)
                        break;
                    double accuracy = performOperationForFile(operation, file);
                    finalAccuracy += accuracy;
                    totalFiles += 1;
                    System.out.println(totalFiles);
                }
            }
        } else {
            double accuracy = performOperationForFile(operation, f);
            finalAccuracy += accuracy;
            totalFiles += 1;
        }
        System.out.println(String.format("Accuracy: %f%%", finalAccuracy*100/totalFiles));

    }
}