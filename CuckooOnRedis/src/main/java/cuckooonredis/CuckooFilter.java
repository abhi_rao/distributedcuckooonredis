package cuckooonredis;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.Level;
import redis.clients.jedis.BinaryJedis;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import cuckooutil.*;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

import static cuckooutil.FilterCommunication.ClusterOpsEnum.BOOTSTRAP;

/**
 * Created by abhijith on 13/2/17.
 */
public class CuckooFilter {

    private static final Logger logger = LogManager.getLogger(CuckooFilter.class.getName());
    private int maxNumKicks;
    private ServerSocket serverSocket;
    private int bucketDepth;
    private HashFunction hashFn;
    public static long MAX_FILTER_SIZE = (1<<16);
    private static int LOAD_BALANCE_K = 16;
    private TreeMap<Long, byte[]> servers;
    private TreeMap<Long, byte[]> futureServers;
    private Map<Long, Lock> lockMap;
    private String ipAddress;
    private short port;
    private Lock cleanupLock;
    private Set<Object> hashSet;
    private JedisPool jedisPool;



    public CuckooFilter(String ipAddress, short port, int maxNumKicks, int bucketDepth) throws IOException {
        this.ipAddress = ipAddress;
        this.port = port;
        this.hashSet = Collections.synchronizedSet(new HashSet<>());
        this.hashFn = Hashing.murmur3_128(123456789);
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        setJedisPoolConfig(jedisPoolConfig);
        this.jedisPool = new JedisPool(jedisPoolConfig, this.ipAddress, this.port, Protocol.DEFAULT_TIMEOUT);
        this.serverSocket = new ServerSocket(this.port + 20000);
        this.maxNumKicks = maxNumKicks;
        this.bucketDepth = bucketDepth;
        this.servers = new TreeMap<>();
        this.futureServers = new TreeMap<>();
        this.lockMap = new ConcurrentHashMap<>();
        this.cleanupLock = new ReentrantLock();

    }
    private void setJedisPoolConfig(JedisPoolConfig config) {
        config.setMaxTotal(1024);
        config.setMaxIdle(1024);
    }

    private boolean lookupAtFutureResponsibleNode(long index, Data data) throws IOException, ClassNotFoundException {
        Map.Entry<Long, byte[]> entry = this.futureServers.ceilingEntry(index);
        entry = entry == null? this.futureServers.firstEntry():entry;
        Node futureResponsibleServer = (Node)Serializer.deserialize(entry.getValue());
        if(this.isCurrentNode(futureResponsibleServer)) {
            logger.trace( "Lookup at Future Responsible Server not needed as its the same node");
            return false;
        }
        Message message = new Message(FilterCommunication.MessageEnum.EXISTS, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        boolean exists = (boolean) sendObject(futureResponsibleServer, filterCommunication);
        logger.trace( String.format("Lookup of %s at Future Responsible Node %s : %s", data, futureResponsibleServer, exists));
        return exists;
    }

    /**
     * Checks if key with properties specified by <code>oData</code> exists in the Cuckoo Filter
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @return true if the key has been found in the Cuckoo Filter else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean lookup(Object oData) throws IOException, ClassNotFoundException {
        try(BinaryJedis binaryJedis = this.jedisPool.getResource()) {
            long lookupCodeBeginTime = 0L, lookupJedisBeginTime = 0L;
            lookupCodeBeginTime = System.nanoTime();
            Data data = (Data) oData;
            logger.trace(String.format("Lookup for f: %s, i1: %d, i2: %d", Arrays.toString(data.f), data.i1, data.i2));
            byte[] f = data.f;
            long i1 = data.i1;
            long i2 = data.i2;
            byte[] bI1 = Serializer.serialize(i1);
            byte[] bI2 = Serializer.serialize(i2);
            Bucket bucket = null;
            Node responsibleServer1 = (Node) Serializer.deserialize(this.getResponsibleNode(i1));
            Node responsibleServer2 = (Node) Serializer.deserialize(this.getResponsibleNode(i2));
            boolean isCurrentNodeResponsible1 = this.isCurrentNode(responsibleServer1), isCurrentNodeResponsible2 = this.isCurrentNode(responsibleServer2);
            boolean isAnyIndexLocal = isCurrentNodeResponsible1 || isCurrentNodeResponsible2;
            logger.trace(String.format("data.f: %s, Node1: %s, Node2: %s", Arrays.toString(f), responsibleServer1.toString(), responsibleServer2.toString()));
            logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
            if (isAnyIndexLocal) {
                if (isCurrentNodeResponsible1) {
                    lookupCodeBeginTime = System.nanoTime();
                    boolean isExistsInFutureResponsibleNode = this.lookupAtFutureResponsibleNode(i1, data);
                    if (isExistsInFutureResponsibleNode) {
                        logger.trace("Lookup succesful at future responsible node");
                        return true;
                    }
                    if (!lockMap.containsKey(i1)) {
                        lockMap.put(i1, new ReentrantLock());
                    }
                    Lock lock = lockMap.get(i1);
                    byte[] bBucket = null;
                    lock.lock();
                    try {
                        logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                        lookupJedisBeginTime = System.nanoTime();
                        bBucket = binaryJedis.get(bI1);
                    } finally {
                        lock.unlock();
                    }

                    logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "JEDIS", System.nanoTime() - lookupJedisBeginTime));
                    lookupCodeBeginTime = System.nanoTime();
                    if (bBucket != null) {
                        bucket = (Bucket) Serializer.deserialize(bBucket);
                        if (bucket.containsF(f)) {
                            logger.trace(String.format("Found at index: %d, node: %s", i1, responsibleServer1.toString()));
                            logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                            return true;
                        } else {
                            logger.trace(String.format("Not found at index: %d, node: %s", i1, responsibleServer1.toString()));
                        }
                    } else {
                        logger.trace(String.format("Bucket at %d was null", i1));
                    }
                    logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                }

                if (isCurrentNodeResponsible2) {
                    lookupCodeBeginTime = System.nanoTime();
                    boolean isExistsInFutureResponsibleNode = this.lookupAtFutureResponsibleNode(i2, data);
                    if (isExistsInFutureResponsibleNode) {
                        logger.trace("Lookup succesful at future responsible node");
                        return true;
                    }
                    if (!lockMap.containsKey(i2)) {
                        lockMap.put(i2, new ReentrantLock());
                    }
                    Lock lock = lockMap.get(i2);
                    lock.lock();
                    byte[] bBucket = null;
                    try {
                        logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                        lookupJedisBeginTime = System.nanoTime();
                        bBucket = binaryJedis.get(bI2);
                        logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "JEDIS", System.nanoTime() - lookupJedisBeginTime));
                    } finally {
                        lock.unlock();
                    }
                    lookupCodeBeginTime = System.nanoTime();
                    if (bBucket != null) {
                        bucket = (Bucket) Serializer.deserialize(bBucket);
                        if (bucket.containsF(f))
                            return true;
                        else {
                            ;
                            logger.trace(String.format("Not found at index: %d, node: %s", i1, responsibleServer1.toString()));
                        }
                    } else {
                        logger.trace(String.format("Bucket at %d was null", i2));
                    }
                    logger.info(String.format("%d,%s,%s,%d", data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                }
                if (isCurrentNodeResponsible1 && isCurrentNodeResponsible2)
                    return false;
            }
            data.retries++;
            if (data.retries > 4) {
                logger.log(Level.WARN, "Retries have increased beyond limit.");
                return false;
            }
            if (responsibleServer1.equals(responsibleServer2)) {
                logger.trace(String.format("Lookup at %s", responsibleServer1.toString()));
                boolean isExistsAtResponsibleServer = this.lookupAtNode(responsibleServer1, data);
                logger.trace(String.format("f: %s, loookup at server: %s, result: %s", Arrays.toString(data.f), responsibleServer1.toString(), isExistsAtResponsibleServer));
                return isExistsAtResponsibleServer;
            } else {
                Data data1 = new Data(data.f, data.i1, data.i1, data.retries, data.index);
                boolean isExistsAtResponsibleServer1 = this.lookupAtNode(responsibleServer1, data1);
                logger.trace(String.format("f: %s, loookup at server: %s, result: %s", Arrays.toString(data1.f), responsibleServer1.toString(), isExistsAtResponsibleServer1));
                if (isExistsAtResponsibleServer1) {
                    return true;
                }
                Data data2 = new Data(data.f, data.i2, data.i2, data.retries, data.index);
                boolean isExistsAtResponsibleServer2 = this.lookupAtNode(responsibleServer2, data2);
                logger.trace(String.format("f: %s, loookup at server: %s, result: %s", Arrays.toString(data2.f), responsibleServer2.toString(), isExistsAtResponsibleServer2));
                if (isExistsAtResponsibleServer2) {
                    return true;
                }
            }
        }

        return false;
    }
    
    /**
     * Check the presence of given key with properties specified by <code>oData</code>. exists in Redis instance at the node <code>responsibleServer</code>
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @param responsibleServer is the node where further lookup is to be performed.
     * @return true if the key has been found at <code>responsibleServer</code> else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private boolean lookupAtNode(Node responsibleServer, Object oData) throws IOException, ClassNotFoundException {
        if(this.isCurrentNode(responsibleServer))
            return false;
        Message message = new Message(FilterCommunication.MessageEnum.EXISTS, (Data)oData);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        filterCommunication.operation = message;
        logger.trace( String.format("Looking up of %s at Node %s", (Data) oData, responsibleServer));
        Boolean isContains = (Boolean) sendObject(responsibleServer, filterCommunication);
        return isContains;
    }


    public boolean isCurrentNode(Node node) {
        return node.equals(new Node(this.ipAddress, this.port));
    }

    /**
     * key with properties specified by <code>oData</code> is to be deleted from the Cuckoo Filter
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @return true if the key has been deleted from the Cuckoo Filter else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean delete(Object oData) throws IOException, ClassNotFoundException {
        try(BinaryJedis binaryJedis = this.jedisPool.getResource()) {
            long deleteCodeBeginTime = 0L, deleteJedisBeginTime = 0L;
            deleteCodeBeginTime = System.nanoTime();
            Data data = (Data) oData;
            byte[] f = data.f;
            long i1 = data.i1;
            long i2 = data.i2;
            logger.trace(String.format("Deletion for f: %s, i1: %d, i2: %d", Arrays.toString(data.f), data.i1, data.i2));
            byte[] bI1 = Serializer.serialize(i1);
            byte[] bI2 = Serializer.serialize(i2);
            Bucket bucket = null;
            Node responsibleServer1 = (Node) Serializer.deserialize(this.getResponsibleNode(i1));
            Node responsibleServer2 = (Node) Serializer.deserialize(this.getResponsibleNode(i2));
            boolean isCurrentNodeResponsible1 = (responsibleServer1.getPort() == this.port), isCurrentNodeResponsible2 = (responsibleServer2.getPort() == this.port);
            boolean isAnyIndexLocal = isCurrentNodeResponsible1 || isCurrentNodeResponsible2;
            logger.trace(String.format("Node1: %s, Node2: %s", Arrays.toString(f), responsibleServer1.toString(), responsibleServer2.toString()));
            logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "CODE", System.nanoTime() - deleteCodeBeginTime));
            if (isAnyIndexLocal) {
                if (isCurrentNodeResponsible1) {
                    deleteCodeBeginTime = System.nanoTime();
                    boolean isDeletedAtFutureNode = this.deleteAtFutureResponsibleNode(i1, data);
                    if (isDeletedAtFutureNode)
                        return true;
                    if (!lockMap.containsKey(i1)) {
                        lockMap.put(i1, new ReentrantLock());
                    }
                    Lock lock = lockMap.get(i1);
                    lock.lock();
                    try {
                        logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "CODE", System.nanoTime() - deleteCodeBeginTime));
                        deleteJedisBeginTime = System.nanoTime();
                        byte[] bBucket = binaryJedis.get(bI1);
                        if (bBucket != null) {
                            bucket = (Bucket) Serializer.deserialize(bBucket);
                            if (bucket.deleteFingerPrint(f)) {
                                String response = binaryJedis.set(bI1, Serializer.serialize(bucket));

                                logger.trace(response);

                                if (response != null) {
                                    logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "JEDIS", System.nanoTime() - deleteJedisBeginTime));
                                    return true;
                                }
                            }


                        }
                    } finally {
                        lock.unlock();
                    }
                    logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "JEDIS", System.nanoTime() - deleteJedisBeginTime));

                }
                if (isCurrentNodeResponsible2) {
                    deleteCodeBeginTime = System.nanoTime();
                    boolean isDeletedAtFutureNode = this.deleteAtFutureResponsibleNode(i2, data);
                    if (isDeletedAtFutureNode)
                        return true;
                    if (!lockMap.containsKey(i2)) {
                        lockMap.put(i2, new ReentrantLock());
                    }
                    Lock lock = lockMap.get(i2);
                    lock.lock();
                    try {
                        logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "CODE", System.nanoTime() - deleteCodeBeginTime));
                        deleteJedisBeginTime = System.nanoTime();
                        byte[] bBucket = binaryJedis.get(bI2);
                        if (bBucket != null) {
                            bucket = (Bucket) Serializer.deserialize(bBucket);
                            if (bucket.deleteFingerPrint(f)) {
                                String response = binaryJedis.set(bI2, Serializer.serialize(bucket));

                                if (response != null) {
                                    logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "JEDIS", System.nanoTime() - deleteJedisBeginTime));

                                    return true;
                                }
                            }
                        }
                    } finally {
                        lock.unlock();
                    }
                    logger.info(String.format("%d,%s,%s,%d", data.index, "DELETE", "JEDIS", System.nanoTime() - deleteJedisBeginTime));
                }
                if (isCurrentNodeResponsible1 && isCurrentNodeResponsible2)
                    return false;
            }
            data.retries++;
            if (data.retries > 4)
                return false;

            if (responsibleServer1.equals(responsibleServer2)) {
                logger.trace(String.format("Deleting at %s", responsibleServer1.toString()));
                boolean isDeletedAtResponsibleServer = this.deleteAtNode(responsibleServer1, data);
                logger.trace(String.format("f: %s, deletion at server: %s, result: %s", Arrays.toString(data.f), responsibleServer1.toString(), isDeletedAtResponsibleServer));
                return isDeletedAtResponsibleServer;
            } else {
                Data data1 = new Data(data.f, data.i1, data.i1, data.retries, data.index);
                boolean isDeletedAtResponsibleServer1 = this.deleteAtNode(responsibleServer1, data1);
                logger.trace(String.format("f: %s, deletion at server: %s, result: %s", Arrays.toString(data1.f), responsibleServer1.toString(), isDeletedAtResponsibleServer1));
                if (isDeletedAtResponsibleServer1) {
                    return true;
                }
                Data data2 = new Data(data.f, data.i2, data.i2, data.retries, data.index);
                boolean isDeletedAtResponsibleServer2 = this.deleteAtNode(responsibleServer2, data2);
                logger.trace(String.format("f: %s, deletion at server: %s, result: %s", Arrays.toString(data2.f), responsibleServer2.toString(), isDeletedAtResponsibleServer2));
                if (isDeletedAtResponsibleServer2) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean deleteAtFutureResponsibleNode(long index, Data data) throws IOException, ClassNotFoundException {
        Map.Entry<Long, byte[]> entry = this.futureServers.ceilingEntry(index);
        entry = entry == null? this.futureServers.firstEntry():entry;
        Node futureResponsibleServer = (Node)Serializer.deserialize(entry.getValue());
        if(this.isCurrentNode(futureResponsibleServer))
            return false;
        Message message = new Message(FilterCommunication.MessageEnum.DELETE, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        boolean isDeleted = (boolean) sendObject(futureResponsibleServer, filterCommunication);
        return isDeleted;
    }


    /**
     * The key with properties specified by <code>oData</code>. is to be deleted from the Redis instance at node <code>responsibleServer</code>
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @param responsibleServer is the node where further lookup is to be performed.
     * @return true if the key has been found at <code>responsibleServer</code> else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private  boolean deleteAtNode(Node responsibleServer, Object oData) throws IOException, ClassNotFoundException {
        if(this.isCurrentNode(responsibleServer))
            return false;
        Message message = new Message(FilterCommunication.MessageEnum.DELETE, (Data)oData);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        filterCommunication.operation = message;
        Boolean isContains = (Boolean) sendObject(responsibleServer, filterCommunication);
        return isContains;
    }

    /**
     * The key with properties specified by <code>oData</code> is to be inserted in the Cuckoo Filter
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @return true if the key has been successfully inserted in the Cuckoo Filter else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private boolean insert(Object oData) throws Exception {
        long insertCodeBeginTime = System.nanoTime();
        Data data = (Data) oData;
        byte[] f = data.f;
        long i1 = data.i1;
        long i2 = data.i2;
        logger.trace( String.format("Insertion for f: %s, i1: %d, i2: %d", Arrays.toString(data.f), data.i1, data.i2));
        byte[] bI1 = Serializer.serialize(i1);
        byte[] bI2 = Serializer.serialize(i2);
        boolean isSuccess = false;
        Node responsibleServer1 = (Node) Serializer.deserialize(this.getResponsibleNode(i1));
        Node responsibleServer2 = (Node) Serializer.deserialize(this.getResponsibleNode(i2));
        logger.trace( String.format("f: %s, Node1: %s, Node2: %s", Arrays.toString(f), responsibleServer1.toString(), responsibleServer2.toString()));
        boolean isCurrentNodeResponsible1 = this.isCurrentNode(responsibleServer1), isCurrentNodeResponsible2 = this.isCurrentNode(responsibleServer2);
        boolean isAnyIndexLocal = isCurrentNodeResponsible1 || isCurrentNodeResponsible2;
        boolean isHead = Math.random() < 0.5;
        logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));
        if(isAnyIndexLocal) {
            if((isHead && isCurrentNodeResponsible1) || (!isHead && !isCurrentNodeResponsible2)) {
                isSuccess = this.insertAtCurrentNode(bI1, oData);
                return isSuccess;
            } else {
                isSuccess = this.insertAtCurrentNode(bI2, oData);
                return isSuccess;
            }
        }

        if(isHead) {
            data.i2 = data.i1;
            return this.insertAtNode(responsibleServer1, data);
        } else {
            data.i1 = data.i2;
            return this.insertAtNode(responsibleServer2, data);
        }
    }

    private boolean insertAtFutureResponsibleNode(long index, Data data) throws IOException, ClassNotFoundException {

        Map.Entry<Long, byte[]> entry = this.futureServers.ceilingEntry(index);
        entry = entry == null? this.futureServers.firstEntry():entry;
        Node futureResponsibleServer = (Node)Serializer.deserialize(entry.getValue());
        if(futureResponsibleServer.getPort() ==  this.port)
            return false;
        Message message = new Message(FilterCommunication.MessageEnum.SET, data);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        boolean exists = (boolean) sendObject(futureResponsibleServer, filterCommunication);
        return exists;
    }

    /**
     * The key with properties specified by <code>oData</code> is to be inserted in the Cuckoo Filter at <code>bI</code> index.
     * Usually fails if maximum number of cuckoo swaps has been exhausted.
     * @param bI serialized representation of the index in the cuckoo filter where the key has to be inserted.
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @return true if the insertion of the key specified at oData is successful at the current node else returns false.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private boolean insertAtCurrentNode(byte[] bI, Object oData) throws Exception {
        try(BinaryJedis binaryJedis = this.jedisPool.getResource()) {
            long insertCodeBeginTime = 0L, insertJedisBeginTime = 0L;
            insertCodeBeginTime = System.nanoTime();
            long index = (long) Serializer.deserialize(bI);
            if (!lockMap.containsKey(index)) {
                lockMap.put(index, new ReentrantLock());
            }
            Data data = (Data) oData;
            Lock lock = lockMap.get(index);
            lock.lock();
            try {
                logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));
                insertJedisBeginTime = System.nanoTime();
                byte[] bBucketI = binaryJedis.get(bI);
                logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "JEDIS", System.nanoTime() - insertJedisBeginTime));
                insertCodeBeginTime = System.nanoTime();

                boolean isInsertedAtFutureResponsibleNode = this.insertAtFutureResponsibleNode(index, data);
                logger.trace(String.format("i1: %d, i2: %d data: %s", data.i1, data.i2, Arrays.toString(data.f)));
                if(isInsertedAtFutureResponsibleNode)
                    return isInsertedAtFutureResponsibleNode;
                logger.trace(String.format("server size: %d, future servers: %d", this.servers.size(), this.futureServers.size()));
                byte[] f = data.f;
                logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));

                if (bBucketI == null) {
                    Bucket bucketsI = new Bucket(this.bucketDepth);
                    bucketsI.insertFingerPrint(f);
                    insertJedisBeginTime = System.nanoTime();
                    String response = binaryJedis.set(bI, Serializer.serialize(bucketsI));
                    logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "JEDIS", System.nanoTime() - insertJedisBeginTime));
                    logger.trace(response);
                    logger.trace(String.format("index: %d, inserted since null", index));
                    return true;
                } else {
                    Bucket bucketsI = (Bucket) Serializer.deserialize(bBucketI);
                    if (bucketsI.hasAnEmptyEntry()) {

                        bucketsI.insertFingerPrint(f);
                        insertJedisBeginTime = System.nanoTime();
                        String response = binaryJedis.set(bI, Serializer.serialize(bucketsI));
                        logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "JEDIS", System.nanoTime() - insertJedisBeginTime));
                        logger.trace(response);
                        logger.trace(String.format("index: %d, had an empty entry", index));
                        return true;
                    } else {
                        logger.trace(String.format("Phew its a swap i1: %d, i2: %d", data.i1, data.i2));
                        data.retries++;
                        if (data.retries == maxNumKicks) {
                            return false;
                        }
                        insertCodeBeginTime = System.nanoTime();
                        int bucketIndex = bucketsI.getRandomFingerPrintIndex();
                        byte[] alternateF = bucketsI.getFingerPrint(bucketIndex);
                        byte[] node = null;
                        if (index == data.i1) {
                            data.i2 = index ^ this.hash(alternateF);
                            data.i1 = data.i2;
                            node = this.getResponsibleNode(data.i2);
                        } else {
                            data.i1 = index ^ this.hash(alternateF);
                            data.i2 = data.i1;
                            node = this.getResponsibleNode(data.i1);
                        }
                        data.f = alternateF;
                        logger.trace(String.format("Its a swap of %s and %s to %s", Arrays.toString(alternateF), Arrays.toString(f), (Node) Serializer.deserialize(node)));
                        logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));
                        boolean isSuccess = this.insertAtNode((Node) Serializer.deserialize(node), data);
                        if (isSuccess) {
                            insertCodeBeginTime = System.nanoTime();
                            bucketsI.deleteFingerPrint(alternateF);
                            bucketsI.insertFingerPrint(f);
                            logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));
                            insertJedisBeginTime = System.nanoTime();
                            String response = binaryJedis.set(bI, Serializer.serialize(bucketsI));
                            logger.info(String.format("%d,%s,%s,%d", data.index, "INSERT", "JEDIS", System.nanoTime() - insertJedisBeginTime));
                            logger.trace(response);
                            return isSuccess;
                        }
                    }
                }
                return false;
            } finally {
                lock.unlock();
            }
        }
    }



    /**
     * The key with properties specified by <code>oData</code>. is to be inserted in the Redis instance at node <code>responsibleServer</code>
     * @param oData is an instance of Data, with i1, i2, f being the properties of the key.
     * @param responsibleServer is the node where further lookup is to be performed.
     * @return true if the key has been found at <code>responsibleServer</code> else false
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private boolean insertAtNode(Node responsibleServer, Object oData) throws IOException, ClassNotFoundException {
        Message message = new Message(FilterCommunication.MessageEnum.SET, (Data)oData);
        FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
        filterCommunication.operation = message;

        boolean isSuccess = (Boolean) sendObject(responsibleServer, filterCommunication);
        return isSuccess;
    }

    private synchronized boolean bootstrapForAddForAnIndex(long index, byte[] currentNodeData, Node node) throws IOException, ClassNotFoundException {
        try(BinaryJedis binaryJedis = this.jedisPool.getResource()) {
            Map.Entry<Long, byte[]> floorEntry = this.servers.floorEntry(index);
            if (floorEntry == null) {
                floorEntry = this.servers.lastEntry();
            }
            Map.Entry<Long, byte[]> ceilingEntry = this.servers.ceilingEntry(index);
            if (ceilingEntry == null) {
                ceilingEntry = this.servers.firstEntry();
            }

            System.out.println(String.format("%s %s", Serializer.deserialize(ceilingEntry.getValue()).toString(), Serializer.deserialize(currentNodeData).toString()));
            if (Arrays.equals(ceilingEntry.getValue(), currentNodeData)) {
                long ceil = ceilingEntry.getKey();
                long floor = floorEntry.getKey();
                floor++;
                long baseFloor = floor;
                logger.trace(String.format("Moving (%d - %d) of (%d - %d) to Node: %s", floor, index, floor, ceil, node.toString()));
                do {
                    byte[] bI = Serializer.serialize(floor);
                    byte[] bBucket = binaryJedis.get(bI);
                    if (bBucket != null) {
                        Bucket bucket = (Bucket) Serializer.deserialize(bBucket);
                        if (bucket.getCurrentSize() == 0)
                            continue;
                        for (int j = 0; j < bucket.getDepth(); j++) {
                            byte[] bF = bucket.getFingerPrint(j);
                            if (bF == null)
                                continue;
                            long i1 = floor;
                            Data data = new Data(bF, i1, i1, 0);
                            System.out.println(data.toString());
                            Message message = new Message(FilterCommunication.MessageEnum.SET, data);
                            FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
                            boolean result = (boolean) this.sendObject(node, filterCommunication);
                            if (result == false) {
                                System.out.println("Sad");
                                return false;
                            }
                        }
                    }
                    floor = (floor + 1) % MAX_FILTER_SIZE;
                } while (floor != index);
                if(floor == index) {
                    byte[] bI = Serializer.serialize(floor);
                    byte[] bBucket = binaryJedis.get(bI);
                    if (bBucket != null) {
                        Bucket bucket = (Bucket) Serializer.deserialize(bBucket);

                        for (int j = 0; j < bucket.getDepth(); j++) {
                            byte[] bF = bucket.getFingerPrint(j);
                            if (bF == null)
                                continue;
                            long i1 = floor;
                            Data data = new Data(bF, i1, i1, 0);
                            System.out.println(data.toString());
                            Message message = new Message(FilterCommunication.MessageEnum.SET, data);
                            FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.MESSAGE, message);
                            boolean result = (boolean) this.sendObject(node, filterCommunication);
                            if (result == false) {
                                System.out.println("Sad Equality");
                                return false;
                            }
                        }
                    }
                }
                System.out.println(String.format("Done moving (%d - %d) of (%d - %d) to Node: %s", baseFloor, index, baseFloor, ceil, node.toString()));
            }
            return true;
        }
    }

    private synchronized Object bootstrapForAdd(byte[] nodeData) throws IOException, ClassNotFoundException {
        Node node = (Node) Serializer.deserialize(nodeData);
        Node currentNode = new Node(this.ipAddress, this.port);
        byte[] currentNodeData = Serializer.serialize(currentNode);
        long baseIndex = this.consistentHashing(nodeData);
        boolean isSuccess = true;
        for(int i = 0; i < LOAD_BALANCE_K; i++) {
            long index = (baseIndex + (i*(MAX_FILTER_SIZE)/LOAD_BALANCE_K))%MAX_FILTER_SIZE;
            isSuccess &= bootstrapForAddForAnIndex(index, currentNodeData, node);
            if(!isSuccess) {
                // Undo the insertion
                return false;
            }
        }
        for(int i = 0; i < LOAD_BALANCE_K; i++) {
            this.futureServers.put((baseIndex + (i*(MAX_FILTER_SIZE)/LOAD_BALANCE_K))%MAX_FILTER_SIZE, nodeData);
        }
        return isSuccess;
    }
    /**
     * A node that is to be added to the cluster, is added to <code>this.servers</code>  and each server is updated with the new range of keys that they are responsible for
     * @param nodeData is the serialised representation of an instance of <code>Node</code> that contains information regarding the new server.
     * @return updated list of <code>this.servers</code>
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private synchronized Object add(byte[] nodeData) throws IOException, ClassNotFoundException {
        long baseIndex = this.consistentHashing(nodeData);
        for(int i = 0; i < LOAD_BALANCE_K; i++) {
            long index = (baseIndex + (i*(MAX_FILTER_SIZE)/LOAD_BALANCE_K))%MAX_FILTER_SIZE;
            this.servers.put(index, nodeData);
            this.futureServers.put(index, nodeData);
        }
        for (Map.Entry<Long, byte[]> entry : this.servers.entrySet()) {
            System.out.println("index: " + entry.getKey() + " Node: " + Serializer.deserialize(entry.getValue()));
        }
        return this.servers;
    }

    //TODO
    /**
     * A node that is to be removed from the cluster, is removed from <code>this.servers</code> and each server is updated with the new range of keys that they are responsible for.
     * @param nodeData is the serialised representation of an instance of <code>Node</code> that contains information regarding the new server.
     * @return updated list of <code>this.servers</code>
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private synchronized Object remove(byte[] nodeData) {
        long baseIndex = this.consistentHashing(nodeData);
        for(int i = 0; i < LOAD_BALANCE_K; i++) {
            long index = (baseIndex + (i*(MAX_FILTER_SIZE)/LOAD_BALANCE_K))%MAX_FILTER_SIZE;
            if(this.servers.containsKey(index)) {
                this.servers.remove(index);
            }
        }
        return this.servers;
    }

    /**
     * The conistent hash of the key <code>x</code> is obtained.
     * @param x serialized representation of the key <code>x</code>
     * @return conistent hash of the key
     */
    private long consistentHashing(byte[] x) {
        HashCode hashCode = this.hashFn.hashBytes(x);

        return consistentHash(hashCode, MAX_FILTER_SIZE);
    }

    private byte[] getResponsibleNode(long index){
        Map.Entry<Long, byte[]> entry = this.servers.ceilingEntry(index);
        if (entry == null) {
            entry = this.servers.firstEntry();
        }
        byte[] responsibleServer = entry.getValue();
        return responsibleServer;
    }

    /**
     * The request received is parsed and handled by invoking the methods responsible for the request.
     * @param filterCommunication
     * @return an Object that's specific to the type of request received.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws CloneNotSupportedException
     */
    private Object handleFilterCommunication(FilterCommunication filterCommunication) throws Exception {
        Object returnValue = null;
        switch (filterCommunication.type) {
            case CLUSTEROPS:{
                ClusterOps clusterOps = (ClusterOps) filterCommunication.operation;
                switch (clusterOps.clusterOpsType) {
                    case ADD:{
                        Object returnObject = this.add(clusterOps.server);
                        return returnObject;
                    }
                    case REMOVE:{
                        return this.remove(clusterOps.server);
                    }
                    case CLUSTER_STATE:{
                        return this.servers;
                    }
                    case BOOTSTRAP:{
                        return this.bootstrapForAdd(clusterOps.server);

                    }
                }
                break;
            }
            case MESSAGE: {
                returnValue = new Boolean(false);
                Message message = (Message) filterCommunication.operation;
                logger.trace(message.data.toString());
                Message message_i1 = (Message) Serializer.deserialize(Serializer.serialize(message)), message_i2 = (Message) Serializer.deserialize(Serializer.serialize(message)), message_inserted = (Message) Serializer.deserialize(Serializer.serialize(message)), copy_message = (Message) Serializer.deserialize(Serializer.serialize(message));
                message_i1.data = new Data(null, message.data.i1, message.data.i1, 0, message.data.index);
                message_i2.data = new Data(null, message.data.i2, message.data.i2, 0, message.data.index);
                message_inserted.data = new Data(null, message.data.i1, message.data.i2, 0, message.data.index);
                copy_message.data = new Data(copy_message.data.f, message.data.i1, message.data.i2, message.data.retries, message.data.index);
                switch (message.messageType) {
                    case EXISTS: {
                        long lookupCodeBeginTime = System.nanoTime();

                        if (this.hashSet.contains(message_inserted) || this.hashSet.contains(message_i1) || this.hashSet.contains((message_i2)))
                            return false;

                        if (message.data.i1 != message.data.i2) {
                            this.hashSet.add(message_i1);
                            this.hashSet.add(message_i2);
                        } else {
                            this.hashSet.add(message_inserted);
                        }
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "LOOKUP", "CODE", System.nanoTime() - lookupCodeBeginTime));
                        returnValue = this.lookup(copy_message.data);
                        if(returnValue.equals(new Boolean(false))) {
                            System.out.println(message.data);
                        }
                        break;

                    }
                    case SET: {

                        long insertCodeBeginTime = System.nanoTime();
                        if (this.hashSet.contains(message_inserted) || this.hashSet.contains(message_i1) || this.hashSet.contains((message_i2)))
                            return false;

                        if (message.data.i1 != message.data.i2) {
                            this.hashSet.add(message_i1);
                            this.hashSet.add(message_i2);
                        } else {
                            this.hashSet.add(message_inserted);
                        }
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "INSERT", "CODE", System.nanoTime() - insertCodeBeginTime));
                        returnValue = this.insert(copy_message.data);

                        break;
                    }
                    case DELETE: {
                        long deleteCodeBeginTime = System.nanoTime();
                        if (this.hashSet.contains(message_inserted) || this.hashSet.contains(message_i1) || this.hashSet.contains((message_i2)))
                            return false;

                        if (message.data.i1 != message.data.i2) {
                            this.hashSet.add(message_i1);
                            this.hashSet.add(message_i2);
                        } else {
                            this.hashSet.add(message_inserted);
                        }
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "DELETE", "CODE", System.nanoTime() - deleteCodeBeginTime));
                        returnValue = this.delete(copy_message.data);
                        break;

                    }
                }
                long codeBeginTime = System.nanoTime();
                cleanupLock.lock();
                try {
                    logger.trace("Lock Map: " + lockMap.size() + Thread.currentThread().toString());
                    if (message.data.i1 != message.data.i2) {
                        this.hashSet.remove(message_i1);
                        this.hashSet.remove(message_i2);
                    } else {
                        this.hashSet.remove(message_inserted);

                    }
                    // Deletion of lock from lockMap
                    boolean isI1Deletable = true, isI2Deletable = true;
                    for (Object o : hashSet) {
                        Message m = (Message) o;
                        if (m.data.i1 == message.data.i1 || m.data.i2 == message.data.i1)
                            isI1Deletable = false;
                        if (m.data.i1 == message.data.i2 || m.data.i2 == message.data.i2)
                            isI2Deletable = false;
                    }
                    if (isI1Deletable)
                        this.lockMap.remove(message.data.i1);
                    if (isI2Deletable)
                        if (message.data.i1 != message.data.i2)
                            this.lockMap.remove(message.data.i2);
                    if (message.messageType == FilterCommunication.MessageEnum.DELETE)
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "DELETE", "_CODE", System.nanoTime() - codeBeginTime));
                    if (message.messageType == FilterCommunication.MessageEnum.EXISTS)
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "LOOKUP", "_CODE", System.nanoTime() - codeBeginTime));
                    if (message.messageType == FilterCommunication.MessageEnum.SET)
                        logger.info(String.format("%d,%s,%s,%d", message.data.index, "INSERT", "_CODE", System.nanoTime() - codeBeginTime));
                } finally {
                    cleanupLock.unlock();
                }
                return returnValue;
            }
        }
        return returnValue;
    }

    /**
     * Each of the request is obtained from the input stream and result is written to the output stream.
     * @param clientSocket socket at the server end of the communication between client.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws CloneNotSupportedException
     */
    private void handleRequest(Socket clientSocket) throws Exception {
        ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        FilterCommunication filterCommunication = (FilterCommunication) objectInputStream.readObject();
        Object returnValue = this.handleFilterCommunication(filterCommunication);
        objectOutputStream.writeObject(returnValue);
        clientSocket.close();
    }

    /**
     * Accepts a request from any of the client nodes and spawns a new thread to handle it.
     * @throws IOException
     */
    public void acceptRequest() throws IOException {
        while (true) {
            final Socket clientSocket = this.serverSocket.accept();
            new Thread() {
                @Override
                public void run() {
                    try {
                        handleRequest(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    //TODO Cleanup post addition
    /**
     * All the nodes are updated regarding the <code>node</code> being added to the cluster.
     * @param node information regarding the server being added to cluster.
     * @param bootstrapServer a node in the cluster, from which current state in the cluster is obtained.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public void setup(Node node, Node bootstrapServer) throws IOException, ClassNotFoundException, InterruptedException {
        HashSet<byte[]> serverSet = new HashSet<>();
        if(!node.equals(bootstrapServer)) {
            ClusterOps bootstrapClusterOps = new ClusterOps(FilterCommunication.ClusterOpsEnum.CLUSTER_STATE, null);
            FilterCommunication bootstrapFilterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.CLUSTEROPS, bootstrapClusterOps);
            TreeMap<Long, byte[]> treeMap = (TreeMap<Long, byte[]>) sendObject(bootstrapServer, bootstrapFilterCommunication);
            serverSet.addAll(treeMap.values());
        }
        for(byte[] bServer : serverSet)
            this.add(bServer);
        this.add(Serializer.serialize(node));
        System.out.println( String.format("Server Size: %d", this.servers.size()));
        {
            Thread[] threads = new Thread[serverSet.size()];

            int i = 0;
            for(byte[] bServer : serverSet) {
                final Node server = (Node) Serializer.deserialize(bServer);
                ClusterOps clusterOps = new ClusterOps(BOOTSTRAP, Serializer.serialize(node));
                final FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.CLUSTEROPS, clusterOps);
                threads[i] = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Check for consistency ?
                            boolean result = (Boolean) sendObject(server, filterCommunication);
                            System.out.println(server);
                            if(!result) {
                                System.out.println( "Failed " + server.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
                threads[i].start();
                i++;
            }
            for(int j = 0; j < i; j++) {
                threads[j].join();
            }
        }

        {
            Thread[] threads = new Thread[serverSet.size()];
            int i = 0;
            for (byte[] bServer : serverSet) {
                final Node server = (Node) Serializer.deserialize(bServer);
                ClusterOps clusterOps = new ClusterOps(FilterCommunication.ClusterOpsEnum.ADD, Serializer.serialize(node));
                final FilterCommunication filterCommunication = new FilterCommunication(FilterCommunication.FilterCommunicationType.CLUSTEROPS, clusterOps);
                threads[i] = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Check for consistency ?
                            TreeMap<Long, byte[]> treeMap = (TreeMap<Long, byte[]>) sendObject(server, filterCommunication);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
                threads[i].start();
                i++;
            }
            for (int j = 0; j < i; j++) {
                threads[j].join();
            }
        }
        System.out.println("Addition of node complete");

    }

    /**
     * Sends any <code>object</code> to the <code>server</code>.
     * @param server the destination for the <code>object</code>
     * @param object data to be sent.
     * @return response from <code>server</code> towards <code>object</code>
     */
    public static final Object sendObject(Node server, Object object) {
        Socket clientSocket = null;
        Object returnObject = null;
        try {
            clientSocket = new Socket(server.getIpAddress(), server.getPort() + 20000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(clientSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //Check for consistency ?
            returnObject = objectInputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnObject;
    }

    public static long consistentHash(HashCode hashCode, long buckets) {
        return consistentHash(hashCode.asLong(), buckets);
    }

    /**
     * Hash of <code>x</code> to an Long integer
     * @param x Key to be hashed
     * @return Long hash value of the object <code>x</code>
     */
    private Long hash(byte[] x) {
        return Math.abs(Hashing.murmur3_128().hashBytes(x).asLong())%MAX_FILTER_SIZE;
    }

    public static long consistentHash(long input, long buckets) {
        Preconditions.checkArgument(buckets > 0, "buckets must be positive: %s", new Object[]{Long.valueOf(buckets)});
        LinearCongruentialGenerator generator = new LinearCongruentialGenerator(input);
        long candidate = 0;

        while(true) {
            long next = (long)((double)(candidate + 1) / generator.nextDouble());
            if(next < 0 || next >= buckets) {
                return candidate;
            }

            candidate = next;
        }
    }
    
    
    private static final class LinearCongruentialGenerator {
        private long state;

        public LinearCongruentialGenerator(long seed) {
            this.state = seed;
        }

        public double nextDouble() {
            this.state = 2862933555777941757L * this.state + 1L;
            return (double)((this.state >>> 33) + 1) / 2.147483648E9D;
        }
    }

    public static void main(String[] args) throws ParseException, IOException, InterruptedException {
        String ipAddress = "127.0.0.1";
        short port = 6379;
        String bootstrapIpAddress = "127.0.0.1";
        short bootstrapPort = 6379;
        int maxNumKicks = 100;
        int bucketDepth = 4;
        long cuckooFilterSize = 1<<16;
        Options options = new Options();
        options.addOption("ipAddress", true, "IP address on which Redis server listens");
        options.addOption("bootstrapIpAddress", true, "IP address for the bootstrap server");
        options.addOption("port", true, "Port on which Redis server listens");
        options.addOption("bootstrapPort", true, "Port for the bootstrap server");
        options.addOption("maxNumKicks", true, "Maximum number of kicks");
        options.addOption("bucketDepth", true, "Bucket depth");
        options.addOption("cuckooFilterSize", true, "Cuckoo filter size");
        options.addOption("disableLogger", false, "To disable logger");
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        if(commandLine.hasOption("ipAddress")){
            ipAddress = commandLine.getOptionValue("ipAddress");
        }
        if(commandLine.hasOption("port")) {
            port = Short.parseShort(commandLine.getOptionValue("port"));
        }
        if(commandLine.hasOption("bootstrapIpAddress")){
            bootstrapIpAddress = commandLine.getOptionValue("bootstrapIpAddress");
        }
        if(commandLine.hasOption("bootstrapPort")) {
            bootstrapPort = Short.parseShort(commandLine.getOptionValue("bootstrapPort"));
        }
        if(commandLine.hasOption("maxNumKicks")) {
            maxNumKicks = Integer.parseInt(commandLine.getOptionValue("maxNumKicks"));
        }
        if(commandLine.hasOption("bucketDepth")) {
            bucketDepth = Integer.parseInt(commandLine.getOptionValue("bucketDepth"));
        }
        if(commandLine.hasOption("cuckooFilterSize")) {
            cuckooFilterSize = Integer.parseInt(commandLine.getOptionValue("cuckooFilterSize"));
            CuckooFilter.MAX_FILTER_SIZE = cuckooFilterSize;
            int loadBalanceK = 0;

            while(cuckooFilterSize > 0) {
                cuckooFilterSize >>= 1;
                loadBalanceK += 1;
            }
            loadBalanceK = 8;
            CuckooFilter.LOAD_BALANCE_K = loadBalanceK;
        }
        final CuckooFilter cuckooFilter = new CuckooFilter(ipAddress, port, maxNumKicks, bucketDepth);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cuckooFilter.acceptRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            cuckooFilter.setup(new Node(ipAddress, port), new Node(bootstrapIpAddress, bootstrapPort));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        logger.trace( "Done with setup");
        logger.trace( cuckooFilter.servers.toString());
        thread.join();
    }
}