package cuckooutil;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by abhijith on 13/2/17.
 * Represents the Bucket being stored at each index in Redis.
 */
public class Bucket implements Serializable{
    private int depth;
    private int currentSize;
    private byte[][] data;
    private int fingerPrintSize;

    public Bucket(int depth) {
        this.fingerPrintSize = 4;
        this.depth = depth;
        this.currentSize = 0;
        this.data = new byte[this.depth][];
    }

    /**
     * Inserts fingerprint <code>x</code>, into the bucket if space is available.
     * @param x - fingerprint to be inserted
     * @return <code>true</code> if insertion was successful else <code>false</code>
     */
    public boolean insertFingerPrint(byte[] x) {
        if(x == null)
            return false;
        for(int i = 0;  i < this.depth; i++) {
            if(this.data[i] == null) {
                this.data[i] = x;
                this.currentSize++;
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the bucket has a free entry.
     * @return <code>true</code> if the bucket has a free entry else <code>false</code>
     */
    public boolean hasAnEmptyEntry() {
        return this.currentSize < this.depth;
    }


    /**
     * Obtains the fingerprint at <code>i</code>th index in the bucket.
     * @param i - index from the the fingerprint needs to be obtained
     * @return <code>null</code> if the it was an invalid index else return the fingerprint at <code>i</code>th index.
     */
    public byte[] getFingerPrint(int i) {
        if(i >= this.depth)
            return null;
        return this.data[i];
    }

    /**
     * Deletes fingerprint <code>f</code>, from the bucket if present.
     * @param f - fingerprint to be deleted
     * @return <code>true</code> if deletion was successful else <code>false</code>
     */
    public boolean deleteFingerPrint(byte[] f) {
        if(f == null)
            return false;
        for(int i = 0; i < this.depth; i++){
            if(Arrays.equals(f, this.data[i])) {
                this.data[i] = null;
                this.currentSize--;
                return true;
            }
        }
        return false;
    }

    /**
     * Obtains a random fingerprint from the bucket.
     * @return index of the fingerprint
     */
    public int getRandomFingerPrintIndex() throws Exception {
        if(this.currentSize == 0) {
            throw new Exception("No fingerprints present in the bucket");
        }
        int[] filledIndices = new int[this.currentSize];
        int index = 0;
        for(int i = 0; i < this.depth; i++){
            if(!(this.data[i] == null)) {
                filledIndices[index++] = i;
            }
        }
        return filledIndices[(int)(Math.random()*this.currentSize)];
    }

    /**
     * Checks if fingerprint <code>f</code> is present in the bucket.
     * @param f - fingerprint to be checked for presence.
     * @return <code>true</code> if <code>f</code> is present else <code>false</code>
     */
    public boolean containsF(byte[] f) {
        if(this.currentSize == 0 || f == null)
            return false;
        for(int i = 0; i < this.depth; i++){
            if(Arrays.equals(f, this.data[i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the number of elements in the bucket.
     */
    public int getDepth() {
        return this.depth;
    }

    /**
     * @return number of elements in the bucket.
     */
    public int getCurrentSize() {return this.currentSize;}
}
