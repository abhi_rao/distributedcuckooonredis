package cuckooutil;

import java.io.Serializable;

/**
 * Created by abhijith on 22/2/17.
 */
public class ClusterOps implements Serializable {
    public FilterCommunication.ClusterOpsEnum clusterOpsType;
    public byte[] server;

    public ClusterOps(FilterCommunication.ClusterOpsEnum clusterOpsType, byte[] server) {
        this.clusterOpsType = clusterOpsType;
        this.server = server;
    }
}
