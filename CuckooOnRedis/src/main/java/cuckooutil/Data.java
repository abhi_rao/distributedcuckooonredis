package cuckooutil;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by abhijith on 22/2/17.
 */
public class Data implements Serializable{
    public byte[] f;
    public long i1 = 0;
    public long i2 = 0;
    public int retries;
    public long index = 0;
    public Data(byte[] f, long i1, long i2, int retries) {
        this.f = f;
        this.i1 = i1;
        this.i2 = i2;
        this.retries = retries;
    }

    public Data(byte[] f, long i1, long i2, int retries, long index) {
        this.f = f;
        this.i1 = i1;
        this.i2 = i2;
        this.retries = retries;
        this.index = index;
    }

    @Override
    public boolean equals(Object obj) {
        Data otherData = (Data) obj;
        return new EqualsBuilder()
                .append(this.i1, otherData.i1)
                .append(this.i2, otherData.i2)
                .append(this.f, otherData.f)
                .append(this.index, otherData.index)
                .isEquals();
    }

    @Override
    public String toString() {
        return String.format("i1: %d, i2: %d, f: %s, index: %d, retires: %d", this.i1, this.i2, Arrays.toString(this.f),this.index, this.retries);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.i1)
                .append(this.i2)
                .append(this.index)
                .append(this.f).hashCode();
    }
}
