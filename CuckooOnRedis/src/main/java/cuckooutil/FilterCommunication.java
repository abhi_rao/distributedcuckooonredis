package cuckooutil;

import java.io.Serializable;

/**
 * Created by abhijith on 15/2/17.
 */
public class FilterCommunication implements Serializable {
    public FilterCommunicationType type;
    public Object operation;

    public static enum FilterCommunicationType {
        MESSAGE, CLUSTEROPS
    }

    public static enum MessageEnum {
        SET, EXISTS, DELETE
    }

    public enum ClusterOpsEnum {
        ADD, REMOVE, BOOTSTRAP, CLUSTER_STATE
    }

    public FilterCommunication(FilterCommunicationType filterCommunicationType, Object operation) {
        this.type = filterCommunicationType;
        this.operation = operation;
    }
}
