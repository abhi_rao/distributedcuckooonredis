package cuckooutil;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import java.io.Serializable;

/**
 * Created by abhijith on 22/2/17.
 */
public class Message implements Serializable {
    public FilterCommunication.MessageEnum messageType;
    public Data data;

    public Message(FilterCommunication.MessageEnum messageType, Data data) {
        this.messageType = messageType;
        this.data = data;

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.data)
                .append(this.messageType)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Message otherMessage = (Message) obj;
        return new EqualsBuilder()
                .append(this.data, otherMessage.data)
                .append(this.messageType, otherMessage.messageType)
                .isEquals();
    }
}
