package cuckooutil;

import java.io.Serializable;

/**
 * Created by abhijith on 15/2/17.
 */
public class Node implements Serializable{
    private String ipAddress;
    private short port;

    public Node(String ipAddress, short port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public short getPort() {
        return this.port;
    }

    @Override
    public boolean equals(Object oOtherNode) {
        Node otherNode = (Node) oOtherNode;
        if(otherNode.getPort() == this.getPort() && otherNode.getIpAddress().equals(this.getIpAddress()))
            return true;
        return false;
    }

    @Override
    public String toString() {
        return String.format("[ip-address: %s, port: %d]", this.ipAddress, this.port);
    }
}
