package cuckooonredis;

import cuckooutil.Data;
import cuckooutil.Node;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by abrao on 4/18/17.
 */
public class CuckooFilterTest {

    public String randomLineGenerator(int size) {
        char[] charArray = new char[size];
        for(int i = 0; i < size; i++) {
            charArray[i] = (char) ('A' + (int)(Math.random()*26));
        }
        return String.valueOf(charArray);
    }

    @Test
    public void insertOfUniqueElementToCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        assertEquals("Insertion of a unique element into Cuckoo Filter failed", true, cuckooClient.insert(new String[] {"0", randomLineGenerator(64)}));
    }
    @Test
    public void insertOfUniqueElementMultipleTimesToCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        String line = randomLineGenerator(64);
        int index = 0;
        while(cuckooClient.insert(new String[] {new Integer(index).toString(), line})) {
            index++;
        }
        System.out.println(index);
        assertEquals("Insertion of a duplicate element into Cuckoo Filter occured more than twice", true, index <= 4*2);
    }

    @Test
    public void deleteExistingElementFromCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        String[] parts = new String[] {"1", randomLineGenerator(64)};
        boolean insertionReturnValue = cuckooClient.insert(parts);
        assertEquals("Deletion of an existing element into Cuckoo Filter failed", true, cuckooClient.delete(parts));
    }

    @Test
    public void deleteNonExistingElementFromCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        String[] parts = new String[] {"1", randomLineGenerator(64)};
        assertEquals("Deletion of an non existent element into Cuckoo Filter was succesful", false, cuckooClient.delete(parts));
    }

    @Test
    public void lookupExistingElementFromCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        String[] parts = new String[] {"4", randomLineGenerator(64)};
        boolean insertionReturnValue;
        do {
            insertionReturnValue = cuckooClient.insert(parts);
        } while(!insertionReturnValue);
        assertEquals("Lookup of an existing element into Cuckoo Filter failed", true, cuckooClient.lookup(parts));
    }

    @Test
    public void lookupNonExistingElementFromCuckooFilter() throws IOException {
        Node seedNode = new Node("127.0.0.1", (short)6379);
        CuckooClient cuckooClient = new CuckooClient(seedNode);
        String[] parts = new String[] {"4", randomLineGenerator(64)};

        assertEquals("Lookup of an non existing element into Cuckoo Filter was successful", false, cuckooClient.lookup(parts));
    }


}
