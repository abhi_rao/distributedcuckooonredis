package cuckooutil;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by abrao on 4/18/17.
 */
public class BucketTest {

    public void randomByteArrayGenerator(byte[] b) {
        for(int i = 0; i < b.length; i++) {
            b[i] = (byte)((Math.random()*124) + 1);
        }
    }


    @Test
    public void deleteFromAnEmptyBucket() {
        Bucket bucket = new Bucket(4);
        assertEquals("Deletion from an empty bucket failed returned true", false, bucket.deleteFingerPrint(new byte[4]));

    }

    @Test
    public void deleteNonExistentElementFromBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        for(int i = 0; i < depth; i++) {
            byte[] element = new byte[4];
            randomByteArrayGenerator(element);
            bucket.insertFingerPrint(element);
        }
        // None of the array elements are zero's
        byte[] b = new byte[4];
        assertEquals("Deletion of a non existent element from bucket failed returned true", false, bucket.deleteFingerPrint(b));

    }

    @Test
    public void deleteExistentElementFromBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        int indexToBeDeleted = (int)(Math.random()*4);
        byte[] elementToBeDeleted = null;
        for(int i = 0; i < depth; i++) {
            byte[] element = new byte[4];
            randomByteArrayGenerator(element);
            if(i == indexToBeDeleted) {
                elementToBeDeleted = element;
            }
            bucket.insertFingerPrint(element);
        }

        assertEquals("Deletion of an existent element from bucket failed returned false", true, bucket.deleteFingerPrint(elementToBeDeleted));

    }

    @Test
    public void insertionOfElementToNonFullBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        byte[] elementToBeInserted = new byte[4];
        randomByteArrayGenerator(elementToBeInserted);
        assertEquals("Insertion of an element to non full bucket failed returned false", true, bucket.insertFingerPrint(elementToBeInserted));
    }

    @Test
    public void insertionOfNullToBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        assertEquals("Insertion of null to bucket failed returned true", false, bucket.insertFingerPrint(null));
    }

    @Test
    public void insertionOfElementToFullBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        byte[] elementToBeInserted = new byte[4];
        randomByteArrayGenerator(elementToBeInserted);
        for(int i = 0; i < depth; i++) {
            byte[] element = new byte[4];
            do {
                randomByteArrayGenerator(element);
            }while(!bucket.insertFingerPrint(element));
        }
        assertEquals("Insertion of an element to full bucket failed returned true", false, bucket.containsF(elementToBeInserted));
    }

    @Test
    public void lookupOfExistentElementInBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        int indexToBeDeleted = (int)(Math.random()*4);
        byte[] elementToBeLookedUp = null;
        for(int i = 0; i < depth; i++) {
            byte[] element = new byte[4];
            randomByteArrayGenerator(element);
            if(i == indexToBeDeleted) {
                elementToBeLookedUp = element;
            }
            bucket.insertFingerPrint(element);
        }

        assertEquals("Lookup of an existent elementfailed returned false", true, bucket.containsF(elementToBeLookedUp));
    }

    @Test
    public void lookupOfNullInBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        assertEquals("Lookup of null to bucket returned true", false, bucket.insertFingerPrint(null));
    }

    @Test
    public void lookupOfNonExistentElementInBucket() {
        int depth = 4;
        Bucket bucket = new Bucket(depth);
        byte[] elementToBeInserted = new byte[4];
        randomByteArrayGenerator(elementToBeInserted);
        for(int i = 0; i < depth; i++) {
            byte[] element = new byte[4];
            do {
                randomByteArrayGenerator(element);
            }while(!bucket.insertFingerPrint(element));
        }
        // None of the array elements are zero's
        byte[] b = new byte[4];
        assertEquals("Lookup of an non existent element in the bucket failed returned true", false, bucket.containsF(b));
    }
}
