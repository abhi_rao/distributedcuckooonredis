# Distributed Cuckoo Filter On Redis #

A Cassandra-like distributed Cuckoo filter implementation on Redis

### Prerequisites: ###
* Java
* Redis
* Maven

### Setup: ###
1. git clone https://abhi_rao@bitbucket.org/abhi_rao/distributedcuckooonredis.git
2. cd distributedcuckooonredis/CuckooOnRedis
3. mvn package -Dmaven.test.skip=true

### Running: ###

To run Cuckoo filter on nodes with IP addresses *[ip1, ip2, ..., ipN]*

#### Start Redis server(s) on each node: ####

```

redis-server --port pIJ

	-pIJ - port on which Redis server has to be started

```

#### Start Cuckoo filter on each node running Redis: ####

```

java -cp target/CuckooOnRedis-1.0-SNAPSHOT.jar cuckooonredis.CuckooFilter
						-ipAddress ipI -port pIJ
						-bootstrapIpAddress ipK -bootstrapPort pKL
						-maxNumKicks mN
						-bucketDepth bD
						-cuckooFilterSize cF

	-ipI - IP address of the current node
	-pIJ - port on which the Redis server is running
	-ipK - IP address of a node on which Cuckoo filter is already running (for the first machine, it is same as ipI)
	-pKL - port of a Redis server on the node with IP address ipK on which Cuckoo filter is already running
	-mN  - maximum number of kicks allowed (it is an integer; as mN increases, insertion success rate increases but with an increase in latency)
	-bD  - bucket size (usually a small integer)
	-cF  - size of the distributed Cuckoo filter

```

#### To insert, lookup or delete data on the Cuckoo filter: ####

```

java -cp target/CuckooOnRedis-1.0-SNAPSHOT.jar cuckooonredis.CuckooFilter 
						-ipAddress ipI -port pIJ  
						-filePath fp 
						[-recursive]
						-operation op
						-cuckooFilterSize cF
						-maxTotalKeyCount max

	-ipI        - IP address of the current node		
	-pIJ        - port on which the Redis server is running
	-fp         - path of the source where data is present
	-op         - INSERT/DELETE/LOOKUP
	-recursive  - option present if filePath refers to a directory
	-cF         - size of the distributed Cuckoo filter
	-max        - number of keys the operation has to be performed on

```