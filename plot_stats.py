#!/usr/bin/python

import matplotlib.pyplot as plt

with open("INSERT_stats.csv") as f:
    data = f.read()

dbsize = []
mem = []
time = []
success = []
for line in data.split('\n'):
    if len(line.strip()) == 0:
        continue
    _dbsize, _mem, _time, _success = list(map(long, line.strip().split(',')))
    dbsize.append(_dbsize)
    mem.append(_mem)
    time.append(_time)
    success.append(_success + (0 if len(success) == 0 else success[-1]))

plt.ylabel("Total keys in Redis (DB Size)")
plt.xlabel("Total number of insertions attempted")
plt.plot(dbsize)
plt.show()
plt.ylabel("Memory consumed by Redis in MB")
plt.xlabel("Total number of insertions attempted")
plt.plot(mem)
plt.show()
plt.ylabel("Time in nano seconds")
plt.xlabel("Total number of insertions attempted")
plt.plot(time)
plt.show()
plt.ylabel("Successful insertion into Cuckoo Filter")
plt.xlabel("Total number of insertions attempted")
plt.plot(success)
plt.show()
